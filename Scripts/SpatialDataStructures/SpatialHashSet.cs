﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using Godot;
using Skatomotion.Scripts.Utility;

namespace Skatomotion.Scripts.SpatialDataStructures
{
    public class SpatialHashSet<T> : ISpatialCollection<T>
    {
        private readonly AABB boundingBox;
        private readonly Vector3 cellSize;
        private readonly Dictionary<string, HashSet<T>> cells;
        private readonly Dictionary<string, Client<T>> clients;

        public SpatialHashSet(AABB boundingBox, IntVector3 cellCount)
        {
            this.boundingBox = boundingBox;
            cellSize = boundingBox.Size / (Vector3) cellCount;
            
            // Init the Cells Dictionary with empty HashSet Buckets
            cells = new Dictionary<string, HashSet<T>>();
            for (int x = 0; x < cellCount.X; x++)
            {
                for (int y = 0; y < cellCount.Y; y++)
                {
                    for (int z = 0; z < cellCount.Z; z++)
                    {
                        var key = Key(x,y,z);
                        cells.Add(key, new HashSet<T>());
                    }
                }
            }

            clients = new Dictionary<string, Client<T>>();
        }

        public int Count() => cells.Select(x => x.Value.Count).Aggregate<int>((i, i1) => i+i1);

        public void Update(T obj, Vector3 position)
        {
            Remove(obj, position);
            Insert(obj, position);
        }

        public void Insert(T obj, Vector3 position)
        {
            Client<T> client;
            if (clients.ContainsKey(obj.ToString()))
            {
                client = clients[obj.ToString()];
            }
            else
            {
                client = new Client<T>()
                {
                    Cells = new HashSet<HashSet<T>>()
                };
                clients.Add(obj.ToString(), client);
            }

            IntVector3 cellIndex = GetCellIndex(position);
            if (cellIndex == InvalidIndex) return;
            
            string key = Key(cellIndex);
            cells[key].Add(obj);
            client.Cells.Add(cells[key]);
        }

        public void Remove(T obj, Vector3 position)
        {
            var client = clients[obj.ToString()];
            foreach (var clientCell in client.Cells)
            {
                clientCell.Remove(obj);
            }
            client.Cells.Clear();
        }

        public HashSet<T> Find(Vector3 position)
        {
            IntVector3 cellIndex = GetCellIndex(position);
            if (cellIndex == InvalidIndex) return new HashSet<T>();
            return GetNeighboringCellIndices(cellIndex).SelectMany(FromCell).ToHashSet();
        }

        private IEnumerable<IntVector3> GetNeighboringCellIndices(IntVector3 centerIndex)
        {
            return IntVector3.Normals.Select(x => x + centerIndex).Where(IsValidIndex);
        }

        private HashSet<T> FromCell(IntVector3 cellIndex)
        {
            if (cellIndex == InvalidIndex) return new HashSet<T>();
            return cells[Key(cellIndex)];
        }
        
        private IntVector3 GetCellIndex(Vector3 position)
        {
            IntVector3 index = Vector3Extensions.FloorToIntVector3(
                (position - boundingBox.Position) / cellSize);
            return IsValidIndex(index) ? index : InvalidIndex;
        }

        private bool IsValidIndex(IntVector3 cellIndex)
        {
            IntVector3 maxIndex = Vector3Extensions.FloorToIntVector3(boundingBox.Size / cellSize) - IntVector3.One;
            IntVector3 minIndex = IntVector3.Zero;
            return (cellIndex.X < maxIndex.X && cellIndex.Y < maxIndex.Y && cellIndex.Z < maxIndex.Z
                    && cellIndex.X > minIndex.X && cellIndex.Y > minIndex.Y && cellIndex.Z > minIndex.Z);
        }

        private static string Key(IntVector3 intVector)
        {
            return Key(
                intVector.X,
                intVector.Y,
                intVector.Z
            );
        }
        private static string Key(int x, int y, int z)
        {
            return $"{x}.{y}.{z}";
        }

        private static IntVector3 InvalidIndex => new IntVector3(-1, -1, -1);
    }

    public struct Client<T>
    {
        public HashSet<HashSet<T>> Cells;
    }
}