﻿using System.Collections.Generic;
using Godot;

namespace Skatomotion.Scripts.SpatialDataStructures
{
    public interface ISpatialCollection<T>
    {
        int Count();
        void Update(T obj, Vector3 position);
        void Insert(T obj, Vector3 position);
        void Remove(T obj, Vector3 position);
        HashSet<T> Find(Vector3 position);
    }
}