﻿using System;
using System.Collections.Generic;
using System.Linq;
using Godot;

namespace Skatomotion.Scripts.SpatialDataStructures
{
    public class NaiveHashSet<T> : ISpatialCollection<T>
    {
        private readonly HashSet<T> items;

        public NaiveHashSet() => items = new HashSet<T>();

        public void Update(T obj, Vector3 position) {}

        public void Insert(T obj, Vector3 position) => items.Add(obj);

        public void Remove(T obj, Vector3 position) => items.Remove(obj);

        public HashSet<T> Find(Vector3 position) => items;

        public int Count() => items.Count();
    }
}