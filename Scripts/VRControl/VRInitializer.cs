﻿using Godot;

namespace Skatomotion.VRControl
{
    public static class VRInterfaces
    {
        public const string
            OpenVR = "OpenVR";
    }

    public static class VRManager
    {

        /// <summary>
        /// Finds the correct VR interface and initializes
        /// </summary>
        /// <returns>If Successful</returns>
        public static bool Initialize() {
            var vr = ARVRServer.FindInterface(VRInterfaces.OpenVR);
            if (vr != null && vr.Initialize())
            {
                // Disable VSync as to not cap VR to the monitor refresh rate Manager
                OS.VsyncEnabled = false;
                // Setting the target FPS, configure the physics update to be the same for smoother visual results
                Engine.TargetFps = 90;
                return true;
            }
            return false;
        }
    }
}