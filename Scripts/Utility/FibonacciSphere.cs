﻿using Godot;

namespace Skatomotion.Scripts.Utility
{
    public static class FibonacciSphere
    {
        const int Points = 300;
        public static readonly Vector3[] Directions;

        static FibonacciSphere () {
            Directions = new Vector3[Points];

            float goldenRatio = (1 + Mathf.Sqrt (5)) / 2;
            float angleIncrement = Mathf.Pi * 2 * goldenRatio;

            for (int i = 0; i < Points; i++) {
                float t = (float) i / Points;
                float inclination = Mathf.Acos (1 - 2 * t);
                float azimuth = angleIncrement * i;

                float x = Mathf.Sin (inclination) * Mathf.Cos (azimuth);
                float y = Mathf.Sin (inclination) * Mathf.Sin (azimuth);
                float z = Mathf.Cos (inclination);
                Directions[i] = new Vector3(x, y, z);
            }
        }
    }
}