﻿using Godot;

namespace Skatomotion.Scripts.Utility
{
    public struct IntVector3
    {
        private bool Equals(IntVector3 other)
        {
            return X == other.X && Y == other.Y && Z == other.Z;
        }

        public override bool Equals(object obj)
        {
            return obj is IntVector3 other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = X;
                hashCode = (hashCode * 397) ^ Y;
                hashCode = (hashCode * 397) ^ Z;
                return hashCode;
            }
        }

        public static IntVector3 Zero => new IntVector3(0, 0, 0);
        public static IntVector3 One => new IntVector3(1, 1, 1);

        public static IntVector3 Up => Vector3Extensions.FloorToIntVector3(Vector3.Up);
        public static IntVector3 Down => Vector3Extensions.FloorToIntVector3(Vector3.Down);
        public static IntVector3 Left => Vector3Extensions.FloorToIntVector3(Vector3.Left);
        public static IntVector3 Right => Vector3Extensions.FloorToIntVector3(Vector3.Right);
        public static IntVector3 Forward => Vector3Extensions.FloorToIntVector3(Vector3.Forward);
        public static IntVector3 Back => Vector3Extensions.FloorToIntVector3(Vector3.Back);

        public static readonly IntVector3[] Normals = {Up, Down, Left, Right, Forward, Back};

        public int X;
        public int Y;
        public int Z;

        public IntVector3(int x, int y, int z)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }

        public override string ToString()
        {
            return $"{X}.{Y}.{Z}";
        }

        public static explicit operator Vector3(IntVector3 intVector3)
        {
            return new Vector3(intVector3.X, intVector3.Y, intVector3.Z);
        }

        public static explicit operator string(IntVector3 intVector3)
        {
            return $"{intVector3.X}.{intVector3.Y}.{intVector3.Z}";
        }
        
        public static IntVector3 operator +(IntVector3 left, IntVector3 right)
        {
            return new IntVector3(
                left.X + right.X,
                left.Y + right.Y,
                left.Z + right.Z);
        }
        
        public static IntVector3 operator -(IntVector3 left, IntVector3 right)
        {
            return new IntVector3(
                left.X - right.X,
                left.Y - right.Y,
                left.Z - right.Z);
        }

        public static bool operator ==(IntVector3 left, IntVector3 right)
        {
            return left.X == right.X && left.Y == right.Y && left.Z == right.Z;
        }

        public static bool operator !=(IntVector3 left, IntVector3 right)
        {
            return left.X != right.X && left.Y != right.Y && left.Z != right.Z;
        }

    }
}