﻿using Godot;

namespace Skatomotion.Scripts.Utility
{
    public static class Interpolation {
        
        // Source: https://chicounity3d.wordpress.com/2014/05/23/how-to-lerp-like-a-pro/

        # region Float
        
        public static float Smoothstep(float a, float b, float t)
        {
            t = Mathf.Clamp(t, 0, 1);
            t = t * t * (3f - 2f * t);
            return a + ((b - a) * t);
        }
    
        public static float Smootherstep(float a, float b, float t)
        {
            t = Mathf.Clamp(t, 0, 1);
            t = t * t * t * (t * (6f * t - 15f) + 10f);
            return a + ((b - a) * t);
        }
        
        #endregion

        #region Vector3
        
        public static Vector3 Smoothstep(Vector3 a, Vector3 b, float t)
        {
            t = Mathf.Clamp(t, 0, 1);
            t = t * t * (3f - 2f * t);
            return a + ((b - a) * t);
        }

        public static Vector3 Smootherstep(Vector3 a, Vector3 b, float t)
        {
            t = Mathf.Clamp(t, 0, 1);
            t = t * t * t * (t * (6f * t - 15f) + 10f);
            return a + ((b - a) * t);
        }
        
        /// <summary>
        /// Linearly interpolate between two values by a normalized weight.
        /// </summary>
        public static Vector3 Lerp(Vector3 from, Vector3 to, float weight)
        {
            return new Vector3(
                Mathf.Lerp(from.x, to.x, weight),
                Mathf.Lerp(from.y, to.y, weight),
                Mathf.Lerp(from.z, to.z, weight)
            );
        }

        /// <summary>
        /// Linearly interpolate between two values by a normalized weight.
        /// </summary>
        public static Vector2 Lerp(Vector2 from, Vector2 to, float weight)
        {
            return new Vector2(
                Mathf.Lerp(from.x, to.x, weight),
                Mathf.Lerp(from.y, to.y, weight)
            );
        }
        
        #endregion
        

    }
}