﻿using Godot;
using Skatomotion.Scripts.Boids;

namespace Skatomotion.Scripts.Utility
{
    public static class Vector3Extensions
    {

        public static IntVector3 FloorToIntVector3(Vector3 vector)
        {
            return new IntVector3
            {
                X = Mathf.FloorToInt(vector.x),
                Y = Mathf.FloorToInt(vector.y),
                Z = Mathf.FloorToInt(vector.z),
            };
        }

        public static Vector3 ClampMagnitude(Vector3 vector, float min, float max)
        {
            float magnitude = vector.Length();
            Vector3 direction = vector / magnitude;
            if(magnitude > max) return direction * max;
            if (magnitude < min) return direction * min;
            return vector;
        }

        public static Vector3 ClampMagnitude(Vector3 vector, float max)
        {
            float magnitude = vector.Length();
            Vector3 direction = vector / magnitude;
            if (magnitude > max) return direction * max;
            return vector;
        }

        public static bool IsNaN(Vector3 vector)
        {
            return (float.IsNaN(vector.x) || float.IsNaN(vector.y) || float.IsNaN(vector.z));
        }
    }
}