using Godot;
using System;

namespace Skatomotion.Locomotion
{
    public class Skateboard : Spatial
    {
        [Export]
        public NodePath RiderNode;

        [Export]
        public float MaximumLeanAngle = 30f;

        private Spatial riderSpatial;

        private Spatial mesh;
        private Transform initialMeshTransform;
        

        public override void _Ready()
        {
            if (!RiderNode.IsEmpty())
            {
                riderSpatial = GetNode(RiderNode) as Spatial;
            }
            
            mesh = FindNode("MeshInstance") as Spatial;
            initialMeshTransform = mesh.Transform;
            base._Ready();
        }

        public override void _PhysicsProcess(float delta)
        {
            if (riderSpatial == null) return;
            
            Vector3 rightOfBoard = Transform.origin + Vector3.Right;
            float dot = rightOfBoard.Dot(riderSpatial.Transform.origin) * 2f;
            float angleDelta = Mathf.Clamp(dot, -1, 1) * MaximumLeanAngle;

            mesh.Transform = initialMeshTransform.Rotated(Vector3.Forward, (float) (Math.PI * angleDelta / 180f));
            base._PhysicsProcess(delta);
        }
    }
}


