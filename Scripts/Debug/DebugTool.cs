﻿using Godot;

namespace Skatomotion.Scripts.Debug
{
    [Tool]
    public class DebugTool : LineGeometry
    {
        [Export()]
        public NodePath Start;
        [Export()]
        public NodePath End;

        public override void _Process(float delta)
        {

            base._Process(delta);
            if (Start != null && End != null)
            {
                Spatial start = GetSpatialNode(Start);
                Spatial end = GetSpatialNode(End);
            }
        }

        private Spatial GetSpatialNode(NodePath path)
        {
            return GetNode(path) as Spatial;
        }
    }
}