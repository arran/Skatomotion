﻿using System;
using Godot;

namespace Skatomotion.Scripts.Debug
{
    [Tool]
    public class PointGeometry : ImmediateGeometry
    {
        private int count = 100;
        private float radius = 2f;
        private float angleStart = 0f;
        private float angleRange = 360f;
        private Direction dir = Direction.Up;

        [Export(PropertyHint.Range, "10, 500")]
        public int Count
        {
            get => count;
            set
            {
                count = value;
                ConstructPoints();
            }
        }

        [Export(PropertyHint.Range, "1, 10")]
        public float Radius
        {
            get => radius;
            set
            {
                radius = value;
                ConstructPoints();
            }
        }

        [Export(PropertyHint.Range, "-180, 180")]
        public float AngleStart
        {
            get => angleStart;
            set
            {
                angleStart = value;
                ConstructPoints();
            }
        }

        [Export(PropertyHint.Range, "0, 360")]
        public float AngleRange
        {
            get => angleRange;
            set
            {
                angleRange = value;
                ConstructPoints();
            }
        }

        [Export(PropertyHint.Enum)]
        public Direction Dir
        {
            get => dir;
            set
            {
                dir = value;
                ConstructPoints();
            }
        }

        public enum Direction
        {
            Up,
            Forward,
            Left,
            Right,
            Back,
            Down
        }

        /// <summary>
        /// Golden Angle in Radians
        /// </summary>
        private static float Phi = Mathf.Pi * (3f - Mathf.Sqrt(5f));


        private Vector3[] points;

        public PointGeometry(Vector3[] points)
        {
            this.points = points;
        }

        public PointGeometry()
        {
            ConstructPoints();
        }


        private void ConstructPoints()
        {
            RemoveAllChildren();

            for (int i = 0; i < Count; i++)
            {
                var p = Point(Radius, i, Count, 0f, 1, AngleStart, AngleRange);
                if (Dir != Direction.Up) p = Shift(p, Dir);
                AddSphere(p);
            }
        }

        public static Vector3 Point(float radius, int index, int total, float min = 0f, float max = 1f,
            float angleStartDeg = 0f, float angleRangeDeg = 360)
        {
            // y goes from min (-) to max (+)
            var y = ((index / (total - 1f)) * (max - min) + min) * 2f - 1f;

            // https://stackoverflow.com/a/26127012/2496170

            // radius at y
            var rY = Mathf.Sqrt(1 - y * y);

            // Angle increment
            float theta = Phi * index;

            if (angleStartDeg != 0 || Math.Abs(angleRangeDeg - 360f) > 0.01)
            {
                theta = (theta % (Mathf.Tau));
                theta = theta < 0 ? theta + Mathf.Tau : theta;

                var a1 = Mathf.Deg2Rad(angleStartDeg);
                var a2 = Mathf.Deg2Rad(angleRangeDeg);

                theta = theta * a2 / Mathf.Tau + a1;
            }

            float x = Mathf.Cos(theta) * rY;
            float z = Mathf.Sin(theta) * rY;

            return new Vector3(x, y, z) * radius;
        }


        public static Vector3 Shift(Vector3 p, Direction direction = Direction.Up)
        {
            switch (direction)
            {
                default:
                case Direction.Up: return new Vector3(p.x, p.y, p.z);
                case Direction.Right: return new Vector3(p.y, p.x, p.z);
                case Direction.Left: return new Vector3(-p.y, p.x, p.z);
                case Direction.Down: return new Vector3(p.x, -p.y, p.z);
                case Direction.Forward: return new Vector3(p.x, p.z, p.y);
                case Direction.Back: return new Vector3(p.x, p.z, -p.y);
            }
        }


        public override void _ExitTree()
        {
            base._ExitTree();
            RemoveAllChildren();
        }

        public override void _EnterTree()
        {
            base._EnterTree();
            Begin(Mesh.PrimitiveType.Lines);
            SetNormal(new Vector3(0, 1, 0));
            SetUv(new Vector2(1, 1));
            foreach (Vector3 p in points)
            {
                AddSphere(p);
            }

            End();
        }

        private void RemoveAllChildren()
        {
            foreach (Node child in GetChildren())
            {
                child.QueueFree();
            }
        }

        private void AddSphere(Vector3 position)
        {
            AddChild(new SphereGeometry(position * 10f, Colors.Aqua, 5, 5, 1f));
        }
    }
}