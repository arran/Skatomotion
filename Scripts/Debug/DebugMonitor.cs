using System;
using System.Collections.Generic;
using System.Linq;
using ByteSizeLib;
using Godot;
using Object = Godot.Object;

namespace Skatomotion.Scripts.Debug
{
    public class DebugMonitor : CanvasLayer
    {
        [Export()] public string DefaultLabelTitleColor = "white";
        [Export()] public string DefaultLabelValueColor = "white";
        
        
        private List<DynamicDebugLabel> labels = new List<DynamicDebugLabel>();
        
        public void AddLabel(string displayName, Func<string> predicate) => AddLabel(displayName, DefaultLabelValueColor, predicate);
        
        public void AddLabel(string displayName, string color, Func<string> predicate)
        {
            labels.Add(new DynamicDebugLabel()
            {
                DisplayName = displayName,
                TitleColor = DefaultLabelTitleColor,
                ValueColor = color,
                Predicate = predicate
            });
        }
        
        public override void _Ready()
        {
            base._Ready();
            AddLabel("FPS", "red", () => Engine.GetFramesPerSecond().ToString());
            AddLabel("S-MEM", () => ByteSize.FromBytes(OS.GetStaticMemoryUsage()).MegaBytes + " Mb");
            AddLabel("D-MEM", () => ByteSize.FromBytes(OS.GetDynamicMemoryUsage()).MegaBytes +  " Mb"); 
            AddLabel("DRAWCALLS", () => Performance.GetMonitor(Performance.Monitor.RenderDrawCallsInFrame).ToString() );
        }

        public override void _Process(float delta)
        {
            base._Process(delta);
            RenderTable();
        }
        
        private void RenderTable()
        {
            var cellContent = String.Empty;
            foreach (var label in labels)
            {
                cellContent += label.ToBBCodeRow();
            }
            string tableText = $"[table=2]{cellContent}[/table]";
            GetChild<RichTextLabel>(0).BbcodeText = tableText;
        }
    }

    public struct DynamicDebugLabel
    {
        public string DisplayName;
        public string TitleColor;
        public string ValueColor;
        public Func<String> Predicate;

        public string ToBBCodeRow() => $"$[cell][b]{BBCodeTitle}[/b][/cell][cell]{BBCodeValue}[/cell]";
        public string ToBBCodeLabel() => $"{BBCodeTitle}: {BBCodeValue}";
        private string BBCodeValue => $"[color={ValueColor}]{Predicate()}[/color]";
        private string BBCodeTitle => $"[color={TitleColor}]{DisplayName}[/color]";
        
        public override string ToString()
        {
            return $"{DisplayName}: {Predicate()}";
        }
    }
}


