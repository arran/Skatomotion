﻿using System;
using Godot;

namespace Skatomotion.Scripts.Debug
{
    public class LineGeometry : ImmediateGeometry
    {
        private Vector3[] points;
        private readonly Color color;

        public LineGeometry()
        {
            this.points = Array.Empty<Vector3>();
        }
        
        public LineGeometry(Vector3[] points, Color color)
        {
            this.points = points;
            this.color = color;
        }

        public override void _ExitTree()
        {
            base._ExitTree();
            foreach (Node child in GetChildren())
            {
                child.QueueFree();
            }
        }

        public override void _EnterTree()
        {
            base._EnterTree();

            Begin(Mesh.PrimitiveType.Lines);
            SetNormal(new Vector3(0,1,0));
            SetUv(new Vector2(1,1));
            SetColor(color);
            foreach (Vector3 p in points)
            {
                AddSphere(p);
                AddVertex(p);
            }
            End();

        }

        private void AddSphere(Vector3 position)
        {
            AddChild(new SphereGeometry(position,  color, 5, 5, .25f));
        }
    }
    
    public class SphereGeometry : ImmediateGeometry {
        private readonly Color color;
        private readonly int lat;
        private readonly int lon;
        private readonly float radius;

        public SphereGeometry(Vector3 origin, Color color, int lat, int lon, float radius)
        {
            this.color = color;
            this.lat = lat;
            this.lon = lon;
            this.radius = radius;
            
            Transform = new Transform(Basis.Identity, origin);
        }

        public override void _EnterTree()
        {
            base._EnterTree();
            Clear();
            Begin(Mesh.PrimitiveType.Triangles);
            SetColor(color);
            AddSphere(this.lat, lon, radius);
            End();
        }
    }
}