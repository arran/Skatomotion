﻿using System;
using System.Collections.Generic;
using System.Linq;
using Godot;
using Skatomotion.Scripts.Utility;

namespace Skatomotion.Scripts.Boids
{
    public class Boid : Spatial
    {
        private Vector3 velocity = Vector3.Zero;

        [Export(PropertyHint.ResourceType)]
        public BoidSettings Settings;
        
        private BoidEnvironment environment;
        
        private Vector3 cohesionDelta = Vector3.Zero;
        private Vector3 alignDelta = Vector3.Zero;
        private Vector3 separationDelta = Vector3.Zero;

        public override void _Ready()
        {
            base._Ready();
            environment = GetParent() as BoidEnvironment;
            environment?.RegisterBoid(this);

            GD.Randomize();
            velocity = new Vector3(
                GD.Randf() % Settings.MinimumSpeed,
                GD.Randf() % Settings.MinimumSpeed,
                GD.Randf() % Settings.MinimumSpeed);

            if (Settings.RandomizeColor) ApplyRandomColor();
        }

        private void ApplyRandomColor()
        {
            List<MeshInstance> meshes = GetChildren().OfType<MeshInstance>().ToList();
            var random = new SpatialMaterial();
            random.Set("albedo_color", new Color(GD.Randf() % 255,GD.Randf() % 255, GD.Randf() % 255));
            meshes.ForEach(x => x.MaterialOverride = random);
        }

        public override void _Process(float delta)
        {
            base._Process(delta);
            var acceleration = CalculateBoundaryForce();

            var neighbors = environment.GetNeighbors(this);
            var influencingNeighbors =
            neighbors
                .Where(x => x != this)
                .Where(x => (GlobalTransform.origin - x.GlobalTransform.origin).LengthSquared() <= Mathf.Pow(Settings.AwarenessDistance,2))
                .Take(Settings.MaximumConsideredNeighbors)
                .ToList();

            if (influencingNeighbors.Count >= 1)
            {
                CalculateForces(influencingNeighbors);
                acceleration += SteerTowards(cohesionDelta) * Settings.CohesionPower;
                acceleration += SteerTowards(alignDelta) * Settings.AlignPower;
                acceleration += SteerTowards(separationDelta) * Settings.SeparationPower;
            }
            
            velocity += acceleration;
            velocity = Vector3Extensions.ClampMagnitude(velocity, Settings.MinimumSpeed, Settings.MaximumSpeed);
            
            GlobalTranslate(velocity * delta);
            if (Translation + velocity != Vector3.Up) LookAt( Translation + velocity, Vector3.Up);
            
            environment.UpdateBoid(this);
        }

        public override void _ExitTree()
        {
            base._ExitTree();
            environment.UnRegisterBoid(this);
        }

        private void CalculateForces(List<Boid> neighbors)
        {
            Vector3 centerOfMass = Vector3.Zero;
            Vector3 perceivedVelocity = Vector3.Zero;
            Vector3 avoidanceVector = Vector3.Zero;
            foreach (var boid in neighbors)
            {
                centerOfMass += boid.GlobalTransform.origin;
                perceivedVelocity += boid.velocity;
                
                Vector3 boidDisplacement = boid.GlobalTransform.origin - GlobalTransform.origin;
                if (boidDisplacement.LengthSquared() <= Mathf.Pow(Settings.AvoidanceDistance,2))
                {
                    avoidanceVector += GlobalTransform.origin - boid.GlobalTransform.origin;
                }
            }
            centerOfMass /= neighbors.Count;
            perceivedVelocity /= neighbors.Count;
            
            cohesionDelta = centerOfMass - GlobalTransform.origin;
            alignDelta = perceivedVelocity - velocity;
            separationDelta = avoidanceVector;
        }

        private Vector3 CalculateBoundaryForce()
        {
            // Calculate avoidance force to stay within world boundary
            Vector3 worldCenter = Vector3.Zero;
            float worldBoundaryDistanceSq = (environment.Radius * environment.Radius);
            
            // Normalize distance to boundary
            // < 1 : Within boundary
            // > 1 : Outside boundary 
            // 0 : In the center of the world
            float boundaryAvoidanceMagnitude = 1 - (GlobalTransform.origin.LengthSquared() - worldBoundaryDistanceSq) /
                                                    (-worldBoundaryDistanceSq);

            if (boundaryAvoidanceMagnitude > 0.6f)
            {
                Vector3 displacementDirection = (GlobalTransform.origin - worldCenter).Normalized();
                Vector3 avoidanceDirection = -displacementDirection.Normalized();
                
                // Scale the avoidanceDirection by the boundaryAvoidanceMagnitude
                // Further away from the center, the stronger the avoidance force
                float scaledBoundaryScalar = Mathf.Pow(boundaryAvoidanceMagnitude, 2);
                return avoidanceDirection * Mathf.Max(1,  Math.Abs(scaledBoundaryScalar));
            }
            return Vector3.Zero;
        }

        private Vector3 SteerTowards(Vector3 vector)
        {
            if (vector == Vector3.Zero) return vector;
            Vector3 v = (vector.Normalized() * Settings.MaximumSpeed) - velocity;
            return Vector3Extensions.ClampMagnitude(v, Settings.MaximumSteerForce);
        }
        
    }
}