﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Godot;
using Skatomotion.Scripts.Debug;
using Skatomotion.Scripts.SpatialDataStructures;
using Skatomotion.Scripts.Utility;
using Vector3 = Godot.Vector3;

namespace Skatomotion.Scripts.Boids
{

    public class BoidEnvironment : Node
    {
        [Export()]
        public float Radius = 200f;

        [Export()]
        public int SpatialMapDensity = 10;

        [Export()]
        public string NodePath;
        
        private ISpatialCollection<Boid> map;

        public override void _Ready()
        {
            base._Ready();

            Vector3 environmentOrigin = Vector3.Zero;
            float diameter = Radius * 2;
            AABB aabb = new AABB(environmentOrigin + (-Vector3.One * Radius), Vector3.One * diameter);
            IntVector3 cellSize = new IntVector3(SpatialMapDensity, SpatialMapDensity, SpatialMapDensity);
            map = new SpatialHashSet<Boid>(aabb, cellSize);

            DebugMonitor monitor = GetTree().CurrentScene.GetChildren().OfType<DebugMonitor>().First();
            monitor.AddLabel("Boids", () =>  map.Count().ToString());
            
            var packed = ResourceLoader.Load(NodePath) as PackedScene;
            Spatial x = packed.Instance() as Spatial;
            x.GlobalTranslate(aabb.Position);
            AddChild(x);
            
            Spatial y = packed.Instance() as Spatial;
            y.GlobalTranslate(aabb.End);
            AddChild(y);

        }

        public void RegisterBoid(Boid boid)
        {
            map.Insert(boid, boid.GlobalTransform.origin);
        }

        public void UnRegisterBoid(Boid boid)
        {
            map.Remove(boid, boid.GlobalTransform.origin);
        }

        public List<Boid> GetNeighbors(Boid boid)
        {
            return map.Find(boid.GlobalTransform.origin).ToList();
        }

        public void UpdateBoid(Boid boid)
        {
            map.Update(boid, boid.GlobalTransform.origin);
        }
    }
}