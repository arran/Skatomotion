using System;
using System.Collections.Generic;
using Godot;

namespace Skatomotion.Scripts.Boids
{
    public class Instancer : Spatial
    {
        [Export()] public string InstancePath;

        [Export()] public int TotalInstances = 1;

        [Export()] public int InstancesPerFrame = 10;

        [Export()] public NodePath InstanceParentPath;

        [Export()] public bool IsManualOnly;

        private Node instanceParent;
        private PackedScene packedScene;
        private int instanced = 0;

        private bool isReady => instanceParent != null && packedScene != null && !IsManualOnly;

        public override void _Ready()
        {
            base._Ready();
            packedScene = ResourceLoader.Load(InstancePath) as PackedScene;
            instanceParent = GetNode(InstanceParentPath);
        }

        public override void _Process(float delta)
        {
            base._Process(delta);
            if (isReady) Instantiate();
            if (Input.IsActionJustPressed("ui_accept"))
            {
                InstanceOne();
            }
        }


        private void Instantiate()
        {
            if (instanced < TotalInstances)
            {
                for (int i = 0; i < InstancesPerFrame && i < TotalInstances; i++)
                {
                    InstanceOne();
                }
            }
            else
            {
                QueueFree();
            }
        }

        private void InstanceOne()
        {
            var instance = packedScene.Instance();
            instanceParent.AddChild(instance);
            instanced++;
        }
    }
}