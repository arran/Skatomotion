﻿using Godot;

namespace Skatomotion.Scripts.Boids
{
    public class BoidSettings : Resource
    {
        [Export()] public float MaximumSpeed = 0.8f;
        
        [Export()] public float MinimumSpeed = 0.3f;

        [Export()] public float MaximumSteerForce = 0.2f;

        [Export()] public int MaximumConsideredNeighbors = 5;

        [Export()] public float AwarenessDistance = 2f;

        [Export()] public float AvoidanceDistance = 0.5f;
        
        [Export()] public float CohesionPower = 0.1f;
        
        [Export()] public float SeparationPower = 1f;
        
        [Export()] public float AlignPower = 0.8f;
        
        [Export()] public bool RandomizeColor = true;
    }
}